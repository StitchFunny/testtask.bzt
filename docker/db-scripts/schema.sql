#
# TABLE STRUCTURE FOR: customer
#

CREATE TABLE `customer`
(
    `id`         int(10) unsigned NOT NULL AUTO_INCREMENT,
    `first_name` varchar(100)     NOT NULL,
    `last_name`  varchar(100)     NOT NULL,
    `email`      varchar(255)     NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#
# TABLE STRUCTURE FOR: order
#

CREATE TABLE `order`
(
    `id`            int(10) unsigned NOT NULL AUTO_INCREMENT,
    `customer_id`   int(10) unsigned NOT NULL,
    `purchase_date` datetime         NOT NULL,
    `country`       varchar(50)      NOT NULL,
    `device`        varchar(50)      NOT NULL,
    PRIMARY KEY (`id`),
    KEY `cus_ind` (`customer_id`),
    CONSTRAINT `order_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#
# TABLE STRUCTURE FOR: order_item
#

CREATE TABLE `order_item`
(
    `id`       int(10) unsigned NOT NULL AUTO_INCREMENT,
    `order_id` int(10) unsigned NOT NULL,
    `ean`      char(13)         NOT NULL,
    `quantity` int(10)          NOT NULL,
    `price`    decimal(10, 2)   NOT NULL,
    PRIMARY KEY (`id`),
    KEY `or_ind` (`order_id`),
    CONSTRAINT `order_item_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;
