# Practical Task
It's a simple project designed to display statistic data.
For this realization was added a generator of random statistic data (both add and remove), the generator can be configured.
Also, I let out of next sights: csrf protection, session, PSR-7.
The most interesting in this task were creating a generator and adding missing dates to statistics.

## Live demo
- [http://139.59.140.12/](http://139.59.140.12/)

## Configuration
- Before installation check configuration in .env in root folder
- check configuration in ./project/config/config-variables.php

## Installation
 - Builds, (re)creates, starts, and attaches to containers for a service.
``` sh
$ docker-compose up --build -d
```

### 2. Manual
- create database and import schema from file
```
./db-scripts/schema.sql
```
- setup nginx. You can use configuration from
```
./nginx/nginx.conf
```
## Configuration
- a configuration file placed in next folder
```
./project/config/config-variables.php
```
- Here configure a database
- And configure a random data generator

## Add and Remove data
You can add and remove data from main page by appropriate buttons

## Tech
Application uses nexr projects:
- [Bootstrap 4.5](https://getbootstrap.com/)
- [Lightpick](https://wakirin.github.io/Lightpick)
- [Highcharts](https://www.highcharts.com/)
