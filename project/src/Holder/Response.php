<?php

namespace App\Holder;

use App\Interfaces\ResponseInterface;

class Response implements ResponseInterface
{
    /** @var array */
    private $headers = [];

    /** @var string */
    private $version = '1.1';

    /** @var int */
    private $statusCode = 200;

    /** @var string|null */
    private $content;

    /** @var string */
    private $reasonPhrase = '';

    /**
     * {@inheritDoc}
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * {@inheritDoc}
     */
    public function setHeaders(array $headers): ResponseInterface
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getVersion(): string
    {
        return $this->version;
    }

    /**
     * {@inheritDoc}
     */
    public function setVersion(string $version): ResponseInterface
    {
        $this->version = $version;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * {@inheritDoc}
     */
    public function setStatusCode(int $statusCode): ResponseInterface
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * {@inheritDoc}
     */
    public function setContent(?string $content): ResponseInterface
    {
        $this->content = $content;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }

    /**
     * {@inheritDoc}
     */
    public function setReasonPhrase(string $reasonPhrase): ResponseInterface
    {
        $this->reasonPhrase = $reasonPhrase;

        return $this;
    }
}
