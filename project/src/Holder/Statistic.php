<?php

namespace App\Holder;

class Statistic
{
    /** @var float */
    private $revenue = 0;

    /** @var int */
    private $orders = 0;

    /** @var int */
    private $customers = 0;

    /**
     * @return float
     */
    public function getRevenue(): float
    {
        return $this->revenue;
    }

    /**
     * @param float $revenue
     *
     * @return Statistic
     */
    public function setRevenue(float $revenue): Statistic
    {
        $this->revenue = $revenue;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrders(): int
    {
        return $this->orders;
    }

    /**
     * @param int $orders
     *
     * @return Statistic
     */
    public function setOrders(int $orders): Statistic
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @return int
     */
    public function getCustomers(): int
    {
        return $this->customers;
    }

    /**
     * @param int $customers
     *
     * @return Statistic
     */
    public function setCustomers(int $customers): Statistic
    {
        $this->customers = $customers;

        return $this;
    }
}
