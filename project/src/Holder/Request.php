<?php

namespace App\Holder;

use App\Interfaces\RequestInterface;

class Request implements RequestInterface
{
    /** @var string */
    private $url;

    /** @var array */
    private $query;

    /**
     * @param string $url
     * @param array $query
     */
    public function __construct(string $url, array $query = [])
    {
        $this->url = $url;
        $this->query = $query;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * {@inheritDoc}
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * {@inheritDoc}
     */
    public function getQueryByKey(string $key)
    {
        if (array_key_exists($key, $this->query)) {
            return $this->query[$key];
        }

        return null;
    }
}
