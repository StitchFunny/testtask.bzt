<?php

namespace App\Holder;

class StatisticChartDataFactory
{
    /**
     * @param array|Statistic[] $statistics
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return mixed
     */
    public function createByData(array $statistics, \DateTime $dateFrom, \DateTime $dateTo): StatisticChartData
    {
        /** @var \DateTime[] $period */
        $period = $this->createPeriod($dateFrom, $dateTo);
        $chartData = new StatisticChartData();

        foreach ($period as $originalDate) {
            $date = $originalDate->format('Y-m-d');
            $chartData->addDate($originalDate->format('M-d'));

            if (array_key_exists($date, $statistics)) {
                $chartData->addCustomer($statistics[$date]->getCustomers());
                $chartData->addOrder($statistics[$date]->getOrders());
                $chartData->addRevenue($statistics[$date]->getRevenue());
            } else {
                $chartData->addCustomer(0);
                $chartData->addOrder(0);
                $chartData->addRevenue(0);
            }
        }

        return $chartData;
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return \DatePeriod
     */
    private function createPeriod(\DateTime $dateFrom, \DateTime $dateTo): \DatePeriod
    {
        return new \DatePeriod($dateFrom, new \DateInterval('P1D'), $dateTo);
    }
}
