<?php

namespace App\Holder;

class StatisticFilter
{
    public const FROM = 'from';
    public const TO = 'to';

    /** @var \DateTime */
    private $from;

    /** @var \DateTime */
    private $to;

    /**
     * @return \DateTime
     */
    public function getFrom(): \DateTime
    {
        return $this->from;
    }

    /**
     * @param \DateTime $from
     *
     * @return $this
     */
    public function setFrom(\DateTime $from): StatisticFilter
    {
        $this->from = $from;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getTo(): \DateTime
    {
        return $this->to;
    }

    /**
     * @param \DateTime $to
     *
     * @return $this
     */
    public function setTo(\DateTime $to): StatisticFilter
    {
        $this->to = $to;

        return $this;
    }
}
