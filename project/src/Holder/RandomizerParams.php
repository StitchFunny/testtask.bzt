<?php

namespace App\Holder;

class RandomizerParams
{
    public const DATE_FROM = 'dateFrom';
    public const CUSTOMERS = 'customers';
    public const MAX_ORDERS_PER_CUSTOMER = 'orders';
    public const MAX_ORDERS_ITEM_PER_ORDER = 'ordersItem';

    /** @var \DateTime */
    private $dateFrom;

    /** @var int */
    private $customers = 1;

    /** @var int */
    private $maxOrdersPerCustomer = 1;

    /** @var int */
    private $maxOrdersItemPerOrder = 1;

    /**
     * @return \DateTime
     */
    public function getDateFrom(): \DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param \DateTime $dateFrom
     *
     * @return RandomizerParams
     */
    public function setDateFrom(\DateTime $dateFrom): RandomizerParams
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * @return int
     */
    public function getCustomers(): int
    {
        return $this->customers;
    }

    /**
     * @param int $customers
     *
     * @return RandomizerParams
     */
    public function setCustomers(int $customers): RandomizerParams
    {
        $this->customers = $customers;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxOrdersPerCustomer(): int
    {
        return $this->maxOrdersPerCustomer;
    }

    /**
     * @param int $maxOrdersPerCustomer
     *
     * @return RandomizerParams
     */
    public function setMaxOrdersPerCustomer(int $maxOrdersPerCustomer): RandomizerParams
    {
        $this->maxOrdersPerCustomer = $maxOrdersPerCustomer;

        return $this;
    }

    /**
     * @return int
     */
    public function getMaxOrdersItemPerOrder(): int
    {
        return $this->maxOrdersItemPerOrder;
    }

    /**
     * @param int $maxOrdersItemPerOrder
     *
     * @return RandomizerParams
     */
    public function setMaxOrdersItemPerOrder(int $maxOrdersItemPerOrder): RandomizerParams
    {
        $this->maxOrdersItemPerOrder = $maxOrdersItemPerOrder;

        return $this;
    }
}
