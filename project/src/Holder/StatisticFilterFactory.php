<?php

namespace App\Holder;

use App\Interfaces\RequestInterface;

class StatisticFilterFactory
{
    /**
     * @param RequestInterface $request
     *
     * @return StatisticFilter
     * @throws \Exception
     */
    public function createByRequest(RequestInterface $request): StatisticFilter
    {
        $from = $request->getQueryByKey(StatisticFilter::FROM);
        $to = $request->getQueryByKey(StatisticFilter::TO);

        if ($from && $to) {
            try {
                $dateFrom = (new \DateTime($from));
                $dateTo = (new \DateTime($to));
            } catch (\Throwable $e) {
                throw new \Exception('Request params not valid', 406);
            }

            if ($dateTo > (new \DateTime())) {
                $dateTo = (new \DateTime());
            }
        } else {
            $dateFrom = (new \DateTime())->modify('-1 month');
            $dateTo = (new \DateTime());
        }

        return $this->create($dateFrom, $dateTo);
    }

    /**
     * @param \DateTime $dateFrom
     * @param \DateTime $dateTo
     *
     * @return StatisticFilter
     */
    private function create(\DateTime $dateFrom, \DateTime $dateTo): StatisticFilter
    {
        return (new StatisticFilter())
            ->setFrom($dateFrom->setTime(0, 0))
            ->setTo($dateTo->setTime(23, 59, 59));
    }
}
