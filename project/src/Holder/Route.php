<?php

namespace App\Holder;

use App\Interfaces\RouteInterface;

class Route implements RouteInterface
{
    public const PATTERN = 'pattern';
    public const RESOURCE = 'resource';
    public const ACTION = 'action';

    /** @var string */
    private $resource;

    /** @var string */
    private $action;

    /**
     * Route constructor.
     *
     * @param string $resource
     * @param string $action
     */
    public function __construct(string $resource, string $action)
    {
        $this->resource = $resource;
        $this->action = $action;
    }

    /**
     * {@inheritDoc}
     */
    public function getResource(): string
    {
        return $this->resource;
    }

    /**
     * {@inheritDoc}
     */
    public function getAction(): string
    {
        return $this->action;
    }
}
