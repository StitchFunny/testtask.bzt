<?php

namespace App\Holder;

use App\Core\Config;

class RandomizerParamsFactory
{
    /**
     * @return RandomizerParams
     * @throws \Exception
     */
    public static function createByConfig(): RandomizerParams
    {
        $parameters = Config::getInstance();
        $randomizerParams = new RandomizerParams();

        if ($from = $parameters->getParameter(RandomizerParams::DATE_FROM)) {
            $randomizerParams->setDateFrom(new \DateTime($from));
        }

        if ($customers = $parameters->getParameter(RandomizerParams::CUSTOMERS)) {
            $randomizerParams->setCustomers($customers);
        }

        if ($orders = $parameters->getParameter(RandomizerParams::MAX_ORDERS_PER_CUSTOMER)) {
            $randomizerParams->setMaxOrdersPerCustomer($orders);
        }

        if ($items = $parameters->getParameter(RandomizerParams::MAX_ORDERS_ITEM_PER_ORDER)) {
            $randomizerParams->setMaxOrdersItemPerOrder($items);
        }

        return $randomizerParams;
    }
}
