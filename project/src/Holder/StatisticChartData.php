<?php

namespace App\Holder;

class StatisticChartData implements \JsonSerializable
{
    /** @var array|string[] */
    private $dates = [];

    /** @var array|float[] */
    private $revenues = [];

    /** @var array|int[] */
    private $orders = [];

    /** @var array|int[] */
    private $customers = [];

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return [
            'dates' => $this->getDates(),
            'revenues' => $this->getRevenues(),
            'orders' => $this->getOrders(),
            'customers' => $this->getCustomers(),
        ];
    }

    /**
     * @return array|string[]
     */
    public function getDates(): array
    {
        return $this->dates;
    }

    /**
     * @param array|string[] $dates
     *
     * @return StatisticChartData
     */
    public function setDates(array $dates): StatisticChartData
    {
        $this->dates = $dates;

        return $this;
    }

    /**
     * @param string $item
     *
     * @return $this
     */
    public function addDate(string $item): StatisticChartData
    {
        $this->dates[] = $item;

        return $this;
    }

    /**
     * @return array|float[]
     */
    public function getRevenues(): array
    {
        return $this->revenues;
    }

    /**
     * @param array|float[] $revenues
     *
     * @return StatisticChartData
     */
    public function setRevenues(array $revenues): StatisticChartData
    {
        $this->revenues = $revenues;

        return $this;
    }

    /**
     * @param float $item
     *
     * @return $this
     */
    public function addRevenue(float $item): StatisticChartData
    {
        $this->revenues[] = $item;

        return $this;
    }

    /**
     * @return array|int[]
     */
    public function getOrders(): array
    {
        return $this->orders;
    }

    /**
     * @param array|int[] $orders
     *
     * @return StatisticChartData
     */
    public function setOrders(array $orders): StatisticChartData
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * @param int $item
     *
     * @return $this
     */
    public function addOrder(int $item): StatisticChartData
    {
        $this->orders[] = $item;

        return $this;
    }

    /**
     * @return array|int[]
     */
    public function getCustomers(): array
    {
        return $this->customers;
    }

    /**
     * @param array|int[] $customers
     *
     * @return StatisticChartData
     */
    public function setCustomers(array $customers): StatisticChartData
    {
        $this->customers = $customers;

        return $this;
    }

    /**
     * @param int $item
     *
     * @return $this
     */
    public function addCustomer(int $item): StatisticChartData
    {
        $this->customers[] = $item;

        return $this;
    }
}
