<?php

namespace App\Holder;

use App\Interfaces\RenderDataInterface;

class RenderData implements RenderDataInterface
{
    /** @var string */
    private $templateName;

    /** @var array */
    private $parameters = [];

    /**
     * {@inheritDoc}
     */
    public function getTemplateName(): string
    {
        return $this->templateName;
    }

    /**
     * {@inheritDoc}
     */
    public function setTemplateName(string $templateName): RenderData
    {
        $this->templateName = $templateName;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * {@inheritDoc}
     */
    public function setParameters(array $parameters): RenderData
    {
        $this->parameters = $parameters;

        return $this;
    }
}
