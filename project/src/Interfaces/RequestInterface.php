<?php

namespace App\Interfaces;

interface RequestInterface
{
    /**
     * @return string
     */
    public function getUrl(): string;

    /**
     * @return array
     */
    public function getQuery(): array;

    /**
     * @return mixed
     */
    public function getQueryByKey(string $key);
}
