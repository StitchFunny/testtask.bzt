<?php

namespace App\Interfaces;

interface RenderDataInterface
{
    /**
     * @return string
     */
    public function getTemplateName(): string;

    /**
     * @return array
     */
    public function getParameters(): array;
}
