<?php

namespace App\Interfaces;

interface ControllerInterface
{
    /**
     * @param string $action
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     */
    public function run(string $action, RequestInterface $request): ResponseInterface;
}
