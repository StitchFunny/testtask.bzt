<?php

namespace App\Interfaces;

interface ResponseInterface
{
    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @param array $headers
     *
     * @return ResponseInterface
     */
    public function setHeaders(array $headers): ResponseInterface;

    /**
     * @return string
     */
    public function getVersion(): string;

    /**
     * @param string $version
     *
     * @return ResponseInterface
     */
    public function setVersion(string $version): ResponseInterface;

    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @param int $statusCode
     *
     * @return ResponseInterface
     */
    public function setStatusCode(int $statusCode): ResponseInterface;

    /**
     * @return string|null
     */
    public function getContent(): ?string;

    /**
     * @param string|null $content
     *
     * @return ResponseInterface
     */
    public function setContent(?string $content): ResponseInterface;

    /**
     * @return string
     */
    public function getReasonPhrase(): string;

    /**
     * @param string $reasonPhrase
     *
     * @return ResponseInterface
     */
    public function setReasonPhrase(string $reasonPhrase): ResponseInterface;
}
