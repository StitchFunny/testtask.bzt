<?php

namespace App\Interfaces;

interface RouteInterface
{
    /**
     * @return string
     */
    public function getResource(): string;

    /**
     * @return string
     */
    public function getAction(): string;
}
