<?php

namespace App\Core;

use App\Holder\Route;
use App\Interfaces\RouteInterface;

class RouterUtil
{
    /**
     * @param string $url
     *
     * @return Route
     * @throws \Exception
     */
    public static function createRouteByUrl(string $url): RouteInterface
    {
        $routeParams = self::findRouteByUrl($url);

        return new Route($routeParams[Route::RESOURCE], $routeParams[Route::ACTION]);
    }

    /**
     * @param string $url
     *
     * @return array
     * @throws \Exception
     */
    private static function findRouteByUrl(string $url): array
    {
        $routers = (Config::getInstance())->getRouters();

        foreach ($routers as $route) {
            if (preg_match($route[Route::PATTERN], $url)) {
                return $route;
            }
        }

        throw new \Exception("Page Not Found", 404);
    }
}
