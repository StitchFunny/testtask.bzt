<?php

namespace App\Core;

class PdoFactory
{
    /**
     * @param $host
     * @param $port
     * @param $user
     * @param $password
     * @param $db_name
     *
     * @return \PDO
     */
    public static function create($host, $port, $db_name, $user, $password): \PDO
    {
        $pdo = new \PDO("mysql:host=$host;port=$port;dbname=$db_name", $user, $password);
        $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $pdo->exec('SET NAMES "utf8"');

        return $pdo;
    }
}
