<?php

namespace App\Core;

class AbstractModel
{
    /** @var \PDO */
    protected $conn;

    public function __construct()
    {
        $this->conn = (DbConnection::getInstance())->getConnection();
    }
}
