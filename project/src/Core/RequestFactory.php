<?php

namespace App\Core;

use App\Holder\Request;
use App\Interfaces\RequestInterface;

class RequestFactory
{
    public static function createRequestByGlobal(): RequestInterface
    {
        return new Request(
            str_replace('?' . $_SERVER['QUERY_STRING'], '', $_SERVER['REQUEST_URI']),
            $_GET
        );
    }
}
