<?php

namespace App\Core;

use App\Interfaces\ControllerInterface;

class ControllerFactory
{
    public static function createControllerByClass(string $class): ControllerInterface
    {
        if (class_exists($class)) {
            return new $class();
        }

        throw new \Exception("Controller $class Not Found", 500);
    }
}
