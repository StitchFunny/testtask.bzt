<?php

namespace App\Core;

use App\Interfaces\ControllerInterface;
use App\Interfaces\RequestInterface;
use App\Interfaces\ResponseInterface;

abstract class AbstractController implements ControllerInterface
{
    /**
     * {@inheritDoc}
     */
    public function run(string $action, RequestInterface $request): ResponseInterface
    {
        if (method_exists($this, $action)) {
            return $this->$action($request);
        }

        throw new \Exception(
            sprintf("Method %s of %s not exist", $action, get_class($this)),
            500
        );
    }
}
