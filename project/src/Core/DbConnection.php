<?php

namespace App\Core;

final class DbConnection
{
    public const DATABASE_HOST = 'DATABASE_HOST';
    public const DATABASE_USER = 'DATABASE_USER';
    public const DATABASE_PASSWORD = 'DATABASE_PASSWORD';
    public const DATABASE_NAME = 'DATABASE_NAME';
    public const DATABASE_PORT = 'DATABASE_PORT';

    /** @var \PDO */
    public $connection;

    /** @var DbConnection */
    protected static $instance;

    private function __construct()
    {
    }

    /**
     * @return DbConnection
     */
    public static function getInstance(): DbConnection
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __clone()
    {
    }

    /**
     * @throws \Exception
     */
    private function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * @return \PDO
     */
    public function getConnection(): \PDO
    {
        return $this->connection;
    }

    /**
     * @param \PDO $connection
     *
     * @return DbConnection
     */
    public function setConnection(\PDO $connection): DbConnection
    {
        $this->connection = $connection;

        return $this;
    }
}
