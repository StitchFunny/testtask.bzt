<?php

namespace App\Core;

final class Config
{
    /** @var array */
    private $parameters = [];

    /** @var array */
    private $routers = [];

    /** @var Config */
    protected static $instance;

    private function __construct()
    {
    }

    public static function getInstance(): Config
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __clone()
    {
    }

    /**
     * @throws \Exception
     */
    private function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getParameter(string $name)
    {
        return $this->parameters[$name] ?? null;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     *
     * @return Config
     */
    public function setParameters(array $parameters): Config
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @param string $name
     *
     * @return array
     */
    public function getRouter(string $name): array
    {
        return $this->router[$name] ?? [];
    }

    /**
     * @return array
     */
    public function getRouters(): array
    {
        return $this->routers;
    }

    /**
     * @param array $routers
     *
     * @return Config
     */
    public function setRouters(array $routers): Config
    {
        $this->routers = $routers;

        return $this;
    }
}
