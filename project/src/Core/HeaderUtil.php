<?php

namespace App\Core;

use App\Interfaces\ResponseInterface;

class HeaderUtil
{
    public const HTML_CONTENT_TYPE = 'Content-type: text/html;charset=utf-8';
    public const JSON_CONTENT_TYPE = 'Content-type: application/json;charset=utf-8';

    /**
     * @param ResponseInterface $response
     */
    public static function setHeaders(ResponseInterface $response): void
    {
        foreach ($response->getHeaders() as $header) {
            header($header);
        }

        header(
            sprintf('HTTP/%s %s %s', $response->getVersion(), $response->getStatusCode(), $response->getReasonPhrase()),
            true,
            $response->getStatusCode()
        );
    }
}
