<?php

namespace App\Core;

use App\Interfaces\RenderDataInterface;

class RenderUtil
{
    /**
     * @param RenderDataInterface $data
     *
     * @return false|string
     * @throws \Exception
     */
    public static function render(RenderDataInterface $data)
    {
        $path = self::getTemplatePath($data);

        if (file_exists($path = self::getTemplatePath($data))) {
            $parameters = $data->getParameters();
            ob_start();
            include($path);

            return ob_get_clean();
        }

        throw new \Exception('Template Not Found');
    }

    /**
     * @param RenderDataInterface $data
     *
     * @return string
     */
    private static function getTemplatePath(RenderDataInterface $data): string
    {
        return sprintf('%s/../templates/%s.html.php', dirname(__DIR__), $data->getTemplateName());
    }
}
