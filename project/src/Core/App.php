<?php

namespace App\Core;

use App\Holder\RenderData;
use App\Holder\Response;
use App\Interfaces\RequestInterface;
use App\Interfaces\ResponseInterface;

class App
{
    public function handle(RequestInterface $request): ResponseInterface
    {
        try {
            $route = RouterUtil::createRouteByUrl($request->getUrl());
            $controller = ControllerFactory::createControllerByClass($route->getResource());
            $this->initDb();

            $response = $controller->run($route->getAction(), $request);

            if (in_array(HeaderUtil::HTML_CONTENT_TYPE, $response->getHeaders(), true)) {
                $response->setContent(
                    RenderUtil::render(
                        (new RenderData())
                            ->setTemplateName('base')
                            ->setParameters([
                                'content' => $response->getContent(),
                                'apiBaseUrl' => sprintf(
                                    '%s://%s/v1/api',
                                    $_SERVER['REQUEST_SCHEME'],
                                    $_SERVER['HTTP_HOST']
                                )
                            ])
                    )
                );
            }

            return $response;
        } catch (\Throwable $e) {
            if ($e->getCode() === 406) {
                return (new Response())
                    ->setStatusCode(406)
                    ->setContent($e->getMessage())
                    ->setReasonPhrase($e->getMessage())
                    ->setHeaders([HeaderUtil::HTML_CONTENT_TYPE]);
            }

            return (new Response())
                ->setStatusCode(500)
                ->setContent('Something went wrong')
                ->setReasonPhrase('Something went wrong')
                ->setHeaders([HeaderUtil::HTML_CONTENT_TYPE]);
        }
    }

    /**
     * @throws \Exception
     */
    private function initDb(): void
    {
        $parameterBag = Config::getInstance();

        $connection = PdoFactory::create(
            $parameterBag->getParameter(DbConnection::DATABASE_HOST),
            $parameterBag->getParameter(DbConnection::DATABASE_PORT),
            $parameterBag->getParameter(DbConnection::DATABASE_NAME),
            $parameterBag->getParameter(DbConnection::DATABASE_USER),
            $parameterBag->getParameter(DbConnection::DATABASE_PASSWORD)
        );

        $connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        (DbConnection::getInstance())->setConnection($connection);
    }
}
