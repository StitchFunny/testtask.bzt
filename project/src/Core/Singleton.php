<?php

namespace App\Core;

class Singleton
{
    protected static $instance;

    public static function getInstance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __clone()
    {
    }

    private function __wakeup()
    {
        throw new \Exception("Cannot unserialize a singleton.");
    }
}
