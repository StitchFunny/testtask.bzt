<?php

namespace App\Model;

use App\Core\AbstractModel;
use App\Holder\RandomizerParams;
use App\Holder\Statistic;
use App\Holder\StatisticFilter;

class OrderItemModel extends AbstractModel
{
    /**
     * @param StatisticFilter $filter
     *
     * @return array
     */
    public function getStatistic(StatisticFilter $filter): array
    {
        $sql = 'SELECT DATE(o.purchase_date) date, SUM(oi.quantity*oi.price) AS revenue,
            count(DISTINCT o.id) AS orders,
            count(DISTINCT c.id) customers
            FROM `order_item` oi 
            LEFT JOIN `order` o on oi.order_id = o.id
            LEFT JOIN `customer` c on o.customer_id = c.id
            WHERE o.purchase_date BETWEEN :from AND :to 
            GROUP BY DATE(o.purchase_date) ORDER BY date';

        $statement = $this->conn->prepare($sql);
        $statement->execute([
            'from' => $filter->getFrom()->format('Y-m-d H:i:s'),
            'to' => $filter->getTo()->format('Y-m-d H:i:s'),
        ]);

        return $statement->fetchAll(\PDO::FETCH_UNIQUE | \PDO::FETCH_CLASS, Statistic::class);
    }

    /**
     * @param RandomizerParams $params
     * @param int $orders
     *
     * @return int
     * @throws \Exception
     */
    public function addRandom(RandomizerParams $params, int $orders): int
    {
        $counter = 0;

        $orderIds = $this->conn
            ->query("SELECT id FROM `order` ORDER BY id DESC LIMIT $orders")
            ->fetchAll();
        $orderIds = array_column($orderIds, 'id');

        foreach ($orderIds as $orderId) {
            $orderItems = random_int(1, $params->getMaxOrdersItemPerOrder());

            for ($n = 0; $n < $orderItems; $n++) {
                $sql = sprintf(
                    "INSERT INTO order_item (order_id, ean, quantity, price) VALUES (%s, %s, %s, %s)",
                    $orderId,
                    1234567890123,
                    random_int(1, 5),
                    random_int(1, 100)
                );

                $this->conn->exec($sql);
                $counter++;
            }
        }

        return $counter;
    }
}
