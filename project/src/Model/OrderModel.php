<?php

namespace App\Model;

use App\Core\AbstractModel;
use App\Holder\RandomizerParams;

class OrderModel extends AbstractModel
{
    /**
     * @param RandomizerParams $params
     *
     * @return int
     * @throws \Exception
     */
    public function addRandom(RandomizerParams $params): int
    {
        $customers = $params->getCustomers();
        $from = $params->getDateFrom()->getTimestamp();
        $counter = 0;

        $customersIds = $this->conn
            ->query("SELECT id FROM `customer` ORDER BY id DESC LIMIT $customers")
            ->fetchAll();
        $customersIds = array_column($customersIds, 'id');

        foreach ($customersIds as $customersId) {
            $orders = random_int(1, $params->getMaxOrdersPerCustomer());

            for ($n = 0; $n < $orders; $n++) {
                $sql = sprintf(
                    "INSERT INTO `order` (customer_id, country, device, purchase_date) VALUES (%s, '%s', '%s', '%s')",
                    $customersId,
                    'SE',
                    'desktop',
                    $this->getRandomDate($from)
                );
                $this->conn->exec($sql);
                $counter++;
            }
        }

        return $counter;
    }

    /**
     * @param int $dateFrom
     *
     * @return string
     * @throws \Exception
     */
    private function getRandomDate(int $from): string
    {
        $timestamp = random_int($from, time());
        $date = new \DateTime();
        $date->setTimestamp($timestamp);

        return $date->format('Y-m-d H:i:s');
    }
}
