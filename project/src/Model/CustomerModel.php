<?php

namespace App\Model;

use App\Core\AbstractModel;
use App\Holder\RandomizerParams;

class CustomerModel extends AbstractModel
{
    private const DELETE_AT_TIME = 100;

    /**
     * @param RandomizerParams $params
     *
     * @return int
     */
    public function addRandom(RandomizerParams $params): int
    {
        for ($n = 0; $n < $params->getCustomers(); $n++) {
            $email = 'user' . uniqid($n, false) . '@email.com';
            $this->conn->exec("
                INSERT INTO customer (first_name, last_name, email) VALUES ('First Name', 'Last Name', '{$email}')
            ");
        }

        return $params->getCustomers();
    }

    /**
     * return void
     */
    public function drop(): void
    {
        do {
            $ids = $this->getFirstIds(self::DELETE_AT_TIME);

            if ($ids) {
                $this->deleteByIds($ids);
            }
        } while (count($ids) > 0);
    }

    /**
     * @param array $ids
     *
     * @return int
     */
    private function deleteByIds(array $ids): int
    {
        $normalizedIds = implode(',', $ids);
        $sql = "DELETE FROM `customer` WHERE id IN ($normalizedIds)";
        $statement = $this->conn->query($sql);

        return $statement->rowCount();
    }

    /**
     * @param int $limit
     *
     * @return array
     */
    private function getFirstIds(int $limit): array
    {
        return $this->conn->query("SELECT id FROM `customer` WHERE 1 LIMIT {$limit}")
            ->fetchAll(\PDO::FETCH_COLUMN, 'id');
    }
}
