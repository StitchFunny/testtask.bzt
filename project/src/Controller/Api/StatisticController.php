<?php

namespace App\Controller\Api;

use App\Core\AbstractController;
use App\Core\HeaderUtil;
use App\Holder\RandomizerParamsFactory;
use App\Holder\Response;
use App\Holder\StatisticChartDataFactory;
use App\Holder\StatisticFilterFactory;
use App\Interfaces\RequestInterface;
use App\Interfaces\ResponseInterface;
use App\Model\CustomerModel;
use App\Model\OrderItemModel;
use App\Model\OrderModel;

class StatisticController extends AbstractController
{
    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function index(RequestInterface $request): ResponseInterface
    {
        $filter = (new StatisticFilterFactory())->createByRequest($request);
        $statistics = (new OrderItemModel())->getStatistic($filter);
        $chartData = (new StatisticChartDataFactory())->createByData($statistics, $filter->getFrom(), $filter->getTo());

        return (new Response())
            ->setContent(json_encode($chartData))
            ->setStatusCode(200)
            ->setReasonPhrase('OK')
            ->setHeaders([HeaderUtil::JSON_CONTENT_TYPE]);
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function addRandomData(RequestInterface $request): ResponseInterface
    {
        $randomizeParams = RandomizerParamsFactory::createByConfig();

        $customers = (new CustomerModel())->addRandom($randomizeParams);
        $orders = (new OrderModel())->addRandom($randomizeParams);
        $items = (new OrderItemModel())->addRandom($randomizeParams, $orders);

        $result = sprintf(
            'Added %d customers, %d orders, %d order items from %s. Enjoy!',
            $customers,
            $orders,
            $items,
            $randomizeParams->getDateFrom()->format('d M Y')
        );

        return (new Response())
            ->setStatusCode(201)
            ->setHeaders([HeaderUtil::JSON_CONTENT_TYPE])
            ->setContent(json_encode([
                'message' => $result,
            ]));
    }

    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function dropData(RequestInterface $request): ResponseInterface
    {
        (new CustomerModel())->drop();

        return (new Response())
            ->setStatusCode(200)
            ->setHeaders([HeaderUtil::JSON_CONTENT_TYPE])
            ->setContent(json_encode([
                    'message' => 'All data were deleted',
                ])
            );
    }
}
