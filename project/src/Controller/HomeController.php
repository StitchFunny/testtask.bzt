<?php

namespace App\Controller;

use App\Core\AbstractController;
use App\Core\HeaderUtil;
use App\Core\RenderUtil;
use App\Holder\RenderData;
use App\Holder\Response;
use App\Interfaces\RequestInterface;
use App\Interfaces\ResponseInterface;

class HomeController extends AbstractController
{
    /**
     * @param RequestInterface $request
     *
     * @return ResponseInterface
     * @throws \Exception
     */
    public function index(RequestInterface $request): ResponseInterface
    {
        $data = (new RenderData())->setTemplateName('main');

        return (new Response())
            ->setContent(RenderUtil::render($data))
            ->setStatusCode(200)
            ->setHeaders([HeaderUtil::HTML_CONTENT_TYPE]);
    }
}
