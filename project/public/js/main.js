'use strict';

(function(window, $) {
    window.StatisticApp = function(params) {
        this.apiBaseUrl = params.options.apiBaseUrl;
        this.const = {
            DATES: 'dates',
            REVENUES: 'revenues',
            CUSTOMERS: 'customers',
            ORDERS: 'orders'
        };

        $('.' + params.chart.addDataButtonClass).on('click', this.onAddDataClick.bind(this));
        $('.' + params.chart.dropDataButtonClass).on('click', this.onDropDataClick.bind(this));

        this.picker = new Lightpick({
            field: document.getElementsByClassName(params.picker.fieldClass)[0],
            secondField: document.getElementsByClassName(params.picker.secondFieldClass)[0],
            lang: 'en',
            numberOfMonths: 2,
            numberOfColumns: 3,
            startDate: moment().subtract(1, 'month'),
            endDate: moment(),
            maxDate: moment(),
            singleDate: false,
            onSelect: null,
            onOpen: null,
            onClose: this.updateChart.bind(this)
        });

        this.CHARTS_ORDER = {
            [this.const.REVENUES]: 0,
            [this.const.CUSTOMERS]: 1,
            [this.const.ORDERS]: 2,
        };

        this.chart = Highcharts.chart(params.chart.containerId, {
            chart: {
                type: 'spline',
                events: {
                    load: this.updateChart.bind(this)
                }
            },
            title: {
                text: 'Statistic',
                align: 'left'
            },
            xAxis: [{
                categories: [],
            }],
            yAxis: [{
                title: { text: this.const.REVENUES },
            }, {
                title: { text: this.const.CUSTOMERS },
                allowDecimals: false,
                opposite: true
            }, {
                title: { text: this.const.ORDERS },
                allowDecimals: false,
                opposite: true
            }],
            tooltip: {
                shared: true
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                x: 80,
                verticalAlign: 'top',
                y: 30,
                floating: true,
                backgroundColor: 'rgba(255,255,255,0.25)'
            },
            series: [{
                name: this.const.REVENUES,
                yAxis: 0,
                data: [],
            }, {
                name: this.const.CUSTOMERS,
                yAxis: 1,
                data: [],
            }, {
                name: this.const.ORDERS,
                yAxis: 2,
                data: []
            }],
        });

        this.chart.series[0].hide();
    };

    $.extend(window.StatisticApp.prototype, {
        onAddDataClick: function(event) {
            event.preventDefault();
            const target = $(event.target);
            const originalText = $(event.target).html();

            this.disableAllInputs();
            this.addSpinner(target, 'Handling...');

            this.addRandomDataRequest().then(response => {
                if (response.message) {
                    this.handleMessage(response.message);
                }

                this.updateChart();
                this.enableAllInputs();
                target.html(originalText);
            });
        },

        onDropDataClick: function(event) {
            event.preventDefault();
            const target = $(event.target);
            const originalText = $(event.target).html();

            this.disableAllInputs();
            this.addSpinner(target, 'Handling...');

            this.dropStatisticDataRequest().then(response => {
                if (response.message) {
                    this.handleMessage(response.message);
                }
                this.updateChart();
                this.enableAllInputs();
                target.html(originalText);
            });
        },

        updateChart: function() {
            const from = this.picker.getStartDate().format('YYYY-MM-DD');
            const to = this.picker.getEndDate().format('YYYY-MM-DD');

            if (from && to) {
                this.loadStatisticDataRequest({ from, to })
                    .then(response => {
                        this.chart.xAxis[0].update({ categories: response[this.const.DATES] }, true);
                        this.chart.series[this.CHARTS_ORDER[this.const.REVENUES]].setData(response[this.const.REVENUES]);
                        this.chart.series[this.CHARTS_ORDER[this.const.ORDERS]].setData(response[this.const.ORDERS]);
                        this.chart.series[this.CHARTS_ORDER[this.const.CUSTOMERS]].setData(response[this.const.CUSTOMERS]);
                    })
            }
        },

        loadStatisticDataRequest: function(options) {
            return $.get(this.apiBaseUrl + '/statistic', options)
                .catch((error) => {
                    this.handleError(error);
                });
        },

        addRandomDataRequest: function(options) {
            return $.get(this.apiBaseUrl + '/statistic/add-random', options)
                .catch((error) => {
                    this.handleError(error);
                });
        },

        dropStatisticDataRequest: function(options) {
            return $.get(this.apiBaseUrl + '/statistic/drop-data', options)
                .catch((error) => {
                    this.handleError(error);
                });
        },

        handleError: function(error) {
            let message = 'Something went wrong';

            if (error.statusText) {
                message = error.statusText;
            }

            $('.alert').addClass('alert-danger').show().text(message);
        },

        handleMessage: function(message) {
            if (message) {
                $('.alert').addClass('alert-success').show().text(message);
            }
        },

        disableAllInputs: function() {
            $('input, button').prop('disabled', true);
        },

        enableAllInputs: function() {
            $('input, button').prop('disabled', false);
        },

        addSpinner: function(target, message) {
            target.html(
                `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> ${ message }`
            );
        }
    });
})(window, jQuery);
