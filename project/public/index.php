<?php

declare(strict_types=1);

use App\Core\App;
use App\Core\RequestFactory;
use App\Core\HeaderUtil;

require dirname(__DIR__) . '/config/autoload.php';
require dirname(__DIR__) . '/config/config-routes.php';
require dirname(__DIR__) . '/config/config-variables.php';

$request = RequestFactory::createRequestByGlobal();
$app = new App();
$response = $app->handle($request);

HeaderUtil::setHeaders($response);

echo $response->getContent();
