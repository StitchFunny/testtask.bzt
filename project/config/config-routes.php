<?php

use App\Controller\Api\StatisticController;
use App\Controller\HomeController;
use App\Core\Config;
use App\Holder\Route;

(Config::getInstance())->setRouters([
    'home' => [
        Route::PATTERN => '/^\/$/i',
        Route::RESOURCE => HomeController::class,
        Route::ACTION => 'index',
    ],
    'api_statistics' => [
        Route::PATTERN => '/^\/v1\/api\/statistic\/?$/i',
        Route::RESOURCE => StatisticController::class,
        Route::ACTION => 'index',
    ],
    'api_statistics_add_random' => [
        Route::PATTERN => '/^\/v1\/api\/statistic\/add-random\/?$/i',
        Route::RESOURCE => StatisticController::class,
        Route::ACTION => 'addRandomData',
    ],
    'api_statistics_drop_data' => [
        Route::PATTERN => '/^\/v1\/api\/statistic\/drop-data\/?$/i',
        Route::RESOURCE => StatisticController::class,
        Route::ACTION => 'dropData',
    ],
]);
