<?php

use App\Core\Config;
use App\Core\DbConnection;
use App\Holder\RandomizerParams;

(Config::getInstance())->setParameters([
    DbConnection::DATABASE_HOST => 'db',
    DbConnection::DATABASE_USER => 'user',
    DbConnection::DATABASE_PASSWORD => 'pass',
    DbConnection::DATABASE_NAME => 'practice_db',
    DbConnection::DATABASE_PORT => '3306',
    RandomizerParams::DATE_FROM => '2020-01-01',
    RandomizerParams::CUSTOMERS => 50,
    RandomizerParams::MAX_ORDERS_PER_CUSTOMER => 50,
    RandomizerParams::MAX_ORDERS_ITEM_PER_ORDER => 5,
]);
