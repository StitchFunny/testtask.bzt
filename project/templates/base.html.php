<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Dashboard</title>
    <link rel="stylesheet" type="text/css" href="./vendor/bootstrap/4.5.0/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="./vendor/lightpick/1.5.2/lightpick.min.css">
    <link rel="stylesheet" type="text/css" href="./vendor/highcharts/highcharts.css">
    <link rel="icon" href="https://getbootstrap.com/docs/4.4/assets/img/favicons/favicon.ico">
</head>
<body>
<main role="main">
    <div class="container">
        <div class="alert" role="alert" style="display:none;"></div>

        <div class="px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
            <h3>Common statistics for orders, revenue, number of customers by a date period</h3>
        </div>
        <?= $parameters['content'] ?>
    </div>
</main>

<script src="./vendor/jquery/jquery-3.5.1.min.js"></script>
<script src="./vendor/popperjs/1.16.0/popper.min.js"></script>
<script src="./vendor/bootstrap/4.5.0/bootstrap.min.js"></script>
<script src="./vendor/momentjs/moment.min.js"></script>
<script src="./vendor/lightpick/1.5.2/lightpick.min.js"></script>
<script src="./vendor/highcharts/highcharts.js"></script>
<script src="./js/main.js"></script>

<script>
    $('.alert').on('click', function (event) {
        $(event.target).hide().text('')
    })

    $(document).ready(function() {
        const statisticApp = new StatisticApp({
            options: {
                apiBaseUrl: '<?= $parameters['apiBaseUrl'] ?>'
            },
            picker: {
                fieldClass: 'js-datepicker-from',
                secondFieldClass: 'js-datepicker-to'
            },
            chart: {
                addDataButtonClass: 'js-add-data',
                dropDataButtonClass: 'js-drop-data',
                containerId: 'chart_container'
            }
        });
    });
</script>

</body>
</html>
