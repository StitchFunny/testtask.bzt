<div class="row">
    <div class="col-md-9 px-md-4">
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text">Date Range</span>
            </div>
            <p class="js-datepicker-result"></p>
            <input type="text" class="form-control js-datepicker-from"/>
            <input type="text" class="form-control js-datepicker-to"/>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 px-md-4">
        <div id="chart_container">
            <div class="spinner-border" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 px-md-4">
        <div class="btn-group-vertical">
            <button type="button" class="btn btn-outline-success btn-sm js-add-data">Add Random Data</button>
            <button type="button" class="btn btn-outline-danger btn-sm js-drop-data">Drop all Data</button>
        </div>
    </div>
</div>
